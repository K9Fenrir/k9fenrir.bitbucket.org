
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";

  // TODO: Potrebno implementirati

  return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija





function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();
	var telesnaVisina = $("#kreirajTelesnaVisina").val();
	var telesnaTeza = $("#kreirajTelesnaTeza").val();

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}, {key: "orgHeight", value: telesnaVisina}, {key: "orgWeight", value: telesnaTeza}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		                $("#dodajMeritevEHR").val(ehrId);
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}
$

function dodajMeritevTeze() {
	$("#kreirajSporocilo").html("");
	sessionId = getSessionId();

	var ehrId = $("#dodajMeritevEHR").val();
	var datumInUra = $("#dodajMeritevDatum").val();
	var telesnaTeza = $("#dodajMeritevTeze").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
			async: false,
		    headers: {"Ehr-Session": sessionId}
		});
		var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    commiter: 'pacient'
		};
		$.ajax({
			async: false,
		    url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
		    success: function (res) {
            	$.ajax({
            		async: false,
					url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
		   			type: 'GET',
		   			headers: {"Ehr-Session": sessionId},
					success: function (data) {
						var party = data.party;
						var dataArray = party.partyAdditionalInfo;
						for (i in dataArray){
							if (dataArray[i].key == "orgHeight"){
								var height = parseFloat(dataArray[i].value);
							}
							if (dataArray[i].key == "orgWeight"){
								var orgWeight = parseFloat(dataArray[i].value);
							}
						}
						var orgIndex = Math.round((orgWeight/(height*height))*100)/100;
						var weight = parseFloat(telesnaTeza);
	    				var bodyMassIndex = Math.round((weight/(height*height))*100)/100;
              
            			var result = '<div class="col-lg-6 col-md-6 col-sm-6"><div class="panel panel-default"> \
				  	            <div class="panel-heading" style="background-color:lightblue"> \
					            	<div class="row"> \
					            	  <div class="alling:middle"><b>Rezultat Zadnje Meritve</b></div> \
					            	</div> \
				            	</div> \
				            	<div class="panel-body">Vaš Index Telesne Mase znaša: <b>' + bodyMassIndex + '</b></br>';
				              
				    	if (orgIndex > 25 && bodyMassIndex > 25){
				    		var maxDifference = orgIndex - 25;
				    		var currentDiffernce = orgIndex - bodyMassIndex;
				    		var percentage = (currentDiffernce/maxDifference)*100;
				    		result += 'Odlično vam gre, a niste še čisto na cilju. Kar tako naprej!</br> \
				    				   Zgornja meja zdravega indeksa telesne mase je 25.0,</br> \
				    				   vaš indeks pa je zaenkrat še vedno previsok.</br> \
				    				   Da bi še napredovali, vam priporočamo, da sledite nasvetom za zdravo</br> \
				    				   izgubo telesne teže, ki jih najdete <a target="_blank" href="https://www.aktivni.si/zdravje/nasveti-in-namigi-za-uspesno-izgubo-telesne-teze/">tukaj</a>.</br>'
				    	}
				    	if (orgIndex < 18.5 && bodyMassIndex < 18.5){
				    		var maxDifference = 18.5 - orgIndex;
				    		var currentDiffernce = bodyMassIndex - orgIndex;
				    		var percentage = (currentDiffernce/maxDifference)*100;
				    		result += 'Odlično vam gre, a niste še čisto na cilju.</br> \
				    				   Spodnja meja zdravega indeksa telesne mase je 18.5,</br> \
				    				   vaš indeks pa je zaenkrat še vedno prenizek.</br> \
				    				   Da bi še napredovali, vam priporočamo, da sledite nasvetom za zdravo</br> \
				    				   pridobitev telesne teže, ki jih najdete <a target="_blank" href="https://www.aktivni.si/fitnes/nasveti/kako-na-zdrav-nacin-pridobiti-kilograme/">tukaj</a>.</br>'
				    	}
				        if (bodyMassIndex <= 25 && bodyMassIndex >= 18.5){
				        	var percentage = 100;
				        	result += 'Čestitke! Dosegli ste zdrav Indeks Telesne Mase!</br> \
				        			   Da bi takšni tudi ostali, vam še naprej priporočamo zdravo \
				        			   in uravnano prehrano ter seveda vsakodnevno telesno aktivnost. \
				        			   Dodatne nasvete lahko najdete <a target="_blank" href="https://www.aktivni.si/zdravje/preventiva/16-nasvetov-za-zdravo-zivljenje/">tukaj</a>.</br>'
				        }
				        percentage = Math.round(percentage*100)/100;
				        result += 'Vaš trenutni napredek na poti do zdravega telesa: ' + percentage + '%</br>'
				        
				              
				        result += '</div></div>\
				        			<div class="progress"> \
									<div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="' + percentage + '" aria-valuemin="0" aria-valuemax="100" style="width: ' + percentage + '%"> \
    										<span class="sr-only">' + percentage +'% Complete (success)</span> \
										 </div> \
									</div></div>';
				    	
				    	
				        $("#appendResultHere").html("");
                		$("#appendResultHere").append(result);
		    		},
            	});
		    },
		    error: function(err) {
		        $("#dodajMeritevTezeSporocilo").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
		    }
		});
	}
}

function prikaziPodatke(){
	$("#appendDataHere").html("");
	sessionId = getSessionId();
	
	var ehrId = $("#dodajMeritevEHR").val();
	var dataArray = [31, 30, 30, 29, 29, 27, 27, 25];
	
	if (!ehrId || ehrId.trim().length == 0) {
		$("#prikaziPodatkeSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek: EHR ID!");
	} else {
		var results = '<div class="panel panel-default"> \
				  		<div class="panel-heading" style="background-color:lightblue"> \
							<div class="row"> \
								<div style="allign:right"><b>Prikaz Dosedanjih Meritev</b></div> \
					   		</div> \
				    	</div> \
				       <div class="panel-body">';
		
		$.ajax({
            async: false,
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
		   	type: 'GET',
		   	headers: {"Ehr-Session": sessionId},
			success: function (data) {
				var party = data.party;
				var dataArray = party.partyAdditionalInfo;
				for (i in dataArray){
					if (dataArray[i].key == "orgHeight"){
						var height = parseFloat(dataArray[i].value);
					}
					if (dataArray[i].key == "orgWeight"){
						var orgWeight = parseFloat(dataArray[i].value);
					}
				}
				var orgIndex = Math.round((orgWeight/(height*height))*100)/100;;
            	
            	var indexArray = [orgIndex];
				$.ajax({
					url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			    	type: 'GET',
			    	headers: {"Ehr-Session": sessionId},
			    	success: function (data) {
						var party = data.party;
							$.ajax({
							    url: baseUrl + "/view/" + ehrId + "/" + "weight",
							    type: 'GET',
							    headers: {"Ehr-Session": sessionId},
							    success: function (res) {
							    	if (res.length > 0) {
								    	results += "<table class='table table-striped " +
		                							"table-hover'><tr><th class='text-left'>Datum in Ura</th><th class='text-middle'>BMI</th>" +
		                    						"<th class='text-right'>Telesna Teža</th></tr>";
								        for (var i in res) {
								        	var measuredWeight = parseFloat(res[i].weight);
								    		var bodyMassIndex = Math.round((measuredWeight/(height*height))*100)/100;
								        	indexArray.push(bodyMassIndex);
								            results += "<tr><td class='text-left'>" + res[i].time + "</td><td class='text-middle'>" + bodyMassIndex +
		                        						"</td><td class='text-right'>" + res[i].weight + " " 	+
		                        			res[i].unit + "</td>";
								        }
								        
								        results += "</table>";
								        results += "</div></div>";
										$("#appendDataHere").html("");
		        						$("#appendDataHere").append(results);
		        						var hideData = document.getElementById('dataDiv');
											hideData.style.display = 'none';
							    	} else {
							    		$("#prikaziPodatkeSporocilo").html(
		                    "<span class='obvestilo label label-warning fade-in'>" +
		                    "Ni podatkov!</span>");
							    	}
							    	var correctedArray = [orgIndex];
							    	for (var i = indexArray.length-1; i > 0; i--){
							    		correctedArray.push(indexArray[i]);
							    	}
							    	drawGraph(correctedArray);
							    },
							    error: function() {
							    	$("#preberiMeritveVitalnihZnakovSporocilo").html(
		                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
		                  JSON.parse(err.responseText).userMessage + "'!");
							    }
							});
			    	},
			    	error: function(err) {
			    		$("#prikaziPodatkeSporocilo").html(
		            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
		            JSON.parse(err.responseText).userMessage + "'!");
			    	}
				});
			},
        });
			  	
	}
}

function drawGraph(dataArray){
		$("#appendChartHere").html("");
		$("#appendChartHere").append('<div class="col-lg-13 col-md-13 col-sm-13"> \
				  <div class="panel panel-default"> \
				  	<div class="panel-heading" style="background-color:lightblue"> \
					  	<div class="row"> \
						  <div class="alling:left"><b>Graf Dosedanjih Meritev</b></div> \
						  <div style="allign:right"><button type="button" class="btn btn-primary btn-xs" onclick="hideOrShowData()">Podrobnosti Meritev</button></div> \
					   	</div> \
				    </div> \
				    	<div class="panel-body"> \
				    		<canvas id="lineChart" height="285" width="520"></canvas> \
				    	</div> \
			  		</div> \
		  		</div>');
		  		
		var CHART = $("#lineChart");
		console.log(CHART);
		var minBMI = [18.5, 18.5, 18.5, 18.5, 18.5, 18.5, 18.5, 18.5, 18.5, 18.5, 18.5, 18.5, 18.5, 18.5, 18.5];
		var maxBMI = [25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0, 25.0];
		let lineChart = new Chart(CHART, {
			type: 'line',
			data: {
				labels: ["Teden 0", "Teden 1", "Teden 2", "Teden 3", "Teden 4", "Teden 5", "Teden 6", "Teden 7", "Teden 8", "Teden 9", "Teden 10"],
				datasets: [{
					label: "Vaš BMI: ",
					fill: false,
					lineTenstion: 0.1,
					backgroundColor: "rgba(150, 0, 192, 0.4)",
					borderColor: "rgba(150, 0, 192, 1)",
					borderCapStyle: "butt",
					borderDash: [],
					borderDashOffset: 0.0,
					borderJoinStyle: "miter",
					pointBorderColor: "rgba(150, 0, 192, 1)",
					pointBackgroundColor: "#fff",
					pointBorderWidth: 1,
					pointHoverRadius: 5,
					pointHoverBackgroundColor: "rgba(150, 0, 192, 1)",
					pointHoverBorderWidth: 2,
					pointRadius: 1,
					pointHitRadius: 10,
					data: dataArray,
				},
				{
					label: "Višja Meja BMI",
					fill: false,
					lineTenstion: 0.1,
					backgroundColor: "rgba(0, 192, 0, 0.4)",
					borderColor: "rgba(0, 192, 0, 1)",
					borderCapStyle: "butt",
					borderDash: [],
					borderDashOffset: 0.0,
					borderJoinStyle: "miter",
					pointBorderColor: "rgba(0, 192, 0, 1)",
					pointBackgroundColor: "#fff",
					pointBorderWidth: 1,
					pointHoverRadius: 5,
					pointHoverBackgroundColor: "rgba(0, 192, 0, 1)",
					pointHoverBorderWidth: 2,
					pointRadius: 1,
					pointHitRadius: 10,
					data: maxBMI
				},
				{
					label: "Nižja Meja BMI",
					fill: false,
					lineTenstion: 0.1,
					backgroundColor: "rgba(0, 192, 192, 0.4)",
					borderColor: "rgba(0, 192, 192, 1)",
					borderCapStyle: "butt",
					borderDash: [],
					borderDashOffset: 0.0,
					borderJoinStyle: "miter",
					pointBorderColor: "rgba(0, 192, 192, 1)",
					pointBackgroundColor: "#fff",
					pointBorderWidth: 1,
					pointHoverRadius: 5,
					pointHoverBackgroundColor: "rgba(0, 192, 192, 1)",
					pointHoverBorderWidth: 2,
					pointRadius: 1,
					pointHitRadius: 10,
					data:minBMI
				}]
			}
		});
}

function generator(){
	for (var i = 0; i < 3; i++){
		generirajIzbranoOsebo(i);
	}
}

function generirajIzbranoOsebo(num){
	$("#appendChartHere").html("");
	$("#appendDataHere").html("");
	sessionId = getSessionId();
	var oseba;
	var app = "";
	if (num == 0){
		var ime = "Robert";
		var priimek = "Baratheon";
		var datumRojstva = "1980-5-5T8:00";
		var telesnaVisina = "2.15";
		var telesnaTeza = "160.0";
		oseba = robertBaratheon;
		app += "<p>Predebel: "
	}
	if (num == 1){
		var ime = "Eddard";
		var priimek = "Stark";
		var datumRojstva = "1981-8-16T8:00";
		var telesnaVisina = "1.78";
		var telesnaTeza = "98.0";
		oseba = eddardStark;
		app += "<p>Normalen: "
	}
	if (num == 2){
		var ime = "Viserys";
		var priimek = "Targarien";
		var datumRojstva = "1994-6-21T8:00";
		var telesnaVisina = "1.80";
		var telesnaTeza = "52";
		oseba = viserysTargarien;
		app += "<p>Presuh: "
	}
/*	
	var name = $('#preberiPredlogoBolnika').val()
	console.log(name);
	if (name == "Robert,Baratheon,2.15,160,1980-5-5T8:00"){
		oseba = robertBaratheon;
	}
	if (name == "Eddard,Stark,1.78,98,1981-8-16T8:00"){
		oseba = eddardStark;
	}
	if (name == "Viserys,Targarien,1.80,52,1994-6-21T8:00"){
		oseba = viserysTargarien;
	}
*/	
	

	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}, {key: "orgHeight", value: telesnaVisina}, {key: "orgWeight", value: telesnaTeza}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		                $("#dodajMeritevEHR").val(ehrId);
		                console.log($("#dodajMeritevEHR").val());
		                
		                for (var i in oseba){
		                	$("#dodajMeritevEHR").val(ehrId);
    						$("#dodajMeritevTeze").val(oseba[i].weight);
    						$("#dodajMeritevDatum").val(oseba[i].time);
    						dodajMeritevTeze();
    						$("#appendResultHere").html("");
		                }
		                app += ehrId + "</p>";
    					$("#testAppend").append(app);
		                
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}

function generirajIzbranoOseboIndiv(){
	$("#appendChartHere").html("");
	$("#appendDataHere").html("");
	sessionId = getSessionId();
	var oseba;

	var name = $('#preberiPredlogoBolnika').val()
	console.log(name);
	if (name == "Robert,Baratheon,2.15,160,1980-5-5T8:00"){
		oseba = robertBaratheon;
	}
	if (name == "Eddard,Stark,1.78,98,1981-8-16T8:00"){
		oseba = eddardStark;
	}
	if (name == "Viserys,Targarien,1.80,52,1994-6-21T8:00"){
		oseba = viserysTargarien;
	}
	
	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
	var datumRojstva = $("#kreirajDatumRojstva").val();
	var telesnaVisina = $("#kreirajTelesnaVisina").val();
	var telesnaTeza = $("#kreirajTelesnaTeza").val();
	
	if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "ehrId", value: ehrId}, {key: "orgHeight", value: telesnaVisina}, {key: "orgWeight", value: telesnaTeza}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'>Uspešno kreiran EHR '" +
                          ehrId + "'.</span>");
		                    $("#preberiEHRid").val(ehrId);
		                }
		                $("#dodajMeritevEHR").val(ehrId);
		                console.log($("#dodajMeritevEHR").val());
		                
		                for (var i in oseba){
		                	$("#dodajMeritevEHR").val(ehrId);
    						$("#dodajMeritevTeze").val(oseba[i].weight);
    						$("#dodajMeritevDatum").val(oseba[i].time);
    						dodajMeritevTeze();
		                }
		                
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!");
		            }
		        });
		    }
		});
	}
}


function hideOrShowData() {
    var x = document.getElementById('dataDiv');
    if (x.style.display === 'none') {
        x.style.display = 'block';
    } else {
        x.style.display = 'none';
    }
}

$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajTelesnaVisina").val(podatki[2]);
    $("#kreirajTelesnaTeza").val(podatki[3]);
    $("#kreirajDatumRojstva").val(podatki[4]);
  });

  

});

var robertBaratheon = [{time: "2017-5-28T9:00", weight: 158}, {time: "2017-6-4T9:00", weight: 153}, {time: "2017-6-11T9:00", weight:150}, {time: "2017-6-18T9:00", weight: 146}, 
					   {time: "2017-6-25T9:00", weight: 141}, {time: "2017-7-2T9:00", weight: 139}, {time: "2017-7-9T9:00", weight: 134}, {time: "2017-7-16T9:00", weight: 129}];
var eddardStark = [{time: "2017-5-28T9:00", weight: 97}, {time: "2017-6-4T9:00", weight: 95}, {time: "2017-6-11T9:00", weight:92}, {time: "2017-6-18T9:00", weight: 92}, 
					   {time: "2017-6-25T9:00", weight: 91},{time: "2017-7-2T9:00", weight: 85},{time: "2017-7-9T9:00", weight: 81},{time: "2017-7-16T9:00", weight: 75}];
var viserysTargarien = [{time: "2017-5-28T9:00", weight: 52}, {time: "2017-6-4T9:00", weight: 53}, {time: "2017-6-11T9:00", weight:54}, {time: "2017-6-18T9:00", weight: 52}, 
					   {time: "2017-6-25T9:00", weight: 53}, {time: "2017-7-2T9:00", weight: 55}, {time: "2017-7-9T9:00", weight: 56}, {time: "2017-7-16T9:00", weight: 58}];				


